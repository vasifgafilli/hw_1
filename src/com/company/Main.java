package com.company;

import java.util.Random;
import java.util.Scanner;

public class Main {

    public static void main(String[] args) {

        System.out.println("Let the game begin!");
        System.out.println("Enter your name:");

        Scanner sc = new Scanner(System.in);
        String name = sc.nextLine();
        Random rnd = new Random();
        int x = rnd.nextInt(100);

        while (true)
        {
            System.out.println("Enter your number:");
            int number = sc.nextInt();
            if (x < number)
            {
                System.out.println("Your number is too big. Please, try again");
            }
            else if (x> number)
            {
                System.out.println("Your number is too small. Please, try again");
            }
            else
            {
                System.out.println("Congratulations, "+ name);
                break;
            }


        }
    }
}
